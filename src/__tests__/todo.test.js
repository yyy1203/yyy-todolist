import {
  mount
} from '@vue/test-utils'
import TodoApp from '../components/todolist/TodoList.vue'
let titems = null
let wrapper = null
// 测试新增todo
test('creates a todo', async () => {
  // 创建一个包含被挂载和渲染的 Vue 组件的 Wrapper
  wrapper = mount(TodoApp)
  // expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1)
  const txtNew = wrapper.get('#newtodo')
  const btn = wrapper.get('#btnAdd')
  for (let i = 0; i < 10; i++) {
    // setValue 可以设置一个文本控件的值并更新 v-model 绑定的数据
    txtNew.setValue('New todo')
    // trigger()可以触发一个事件，这里模拟了点击
    await btn.trigger('click'); // 注意！！！！trigger是异步的
  }

  titems = wrapper.findAll('.titem')
  // await Vue.nextTick()
  expect(titems).toHaveLength(10)
})
// 测试删除todo
test('delete a todo', async () => {
  const delBtns = titems[0].findAll('.btnDel')
  await delBtns[0].trigger('click')
  titems = wrapper.findAll('.titem')
  expect(titems).toHaveLength(9)
})

// 测试修改todo
test('change a todo', async () => {
  const ipt = titems[0].findAll('.ipt')
  await ipt[0].trigger('focus')
  await ipt[0].trigger('keyup.esc')

})
// 测试全部删除
test('delete all todo', async () => {
  wrapper = mount(TodoApp)
  const delAll = wrapper.get('#delAll')
  await delAll.trigger('click')
})