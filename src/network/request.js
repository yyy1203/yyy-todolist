import axios from 'axios'


export default {

   saveTodoItem:(data)=>{
    axios.post('http://81.69.7.51:3010/saveitem',data)
   },
   clearItems:()=>{
    axios.post('http://81.69.7.51:3010/clearitems')
   },
   getAllTodoItems:async ()=>{

    const res=await axios.get('http://81.69.7.51:3010/getallitems')

    return res.data
    //console.log(res.data)
   },
   updateItem:async (data)=>{
      axios.post('http://81.69.7.51:3010/updateitem',data)
   },
   delItem:(data)=>{
      axios.post('http://81.69.7.51:3010/deleteitem',data)
     },

}