import { createStore } from 'vuex'

// 创建一个新的 store 实例
const store = createStore({
  state () {
    return {
      isLogined: false
    }
  },
  mutations: {
    changeLoginState (state,loginState) {
      state.isLogined=loginState
    }
  }
})

export default store