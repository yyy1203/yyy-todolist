import { createRouter, createWebHistory } from 'vue-router'
import Register from '../views/Register.vue'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import TodoList from '../components/todolist/TodoList.vue'
// 1. 定义路由组件.
// 也可以从其他文件导入
// const Register = { template: '<div>Register....</div>' }
// const Login = { template: '<div>Login....</div>' }

//  定义一些路由
// 每个路由都需要映射到一个组件。
const routes = [

{
    path:'/',
    component: Home,
    children:[
        { 
            path: '/todolist',
            component: TodoList
        },
        { 
            path: '/Register',
            component: Register,
            meta:{
                AllowAnonymous:true
            }
            
        },
        {
            path: '/Login', 
            component: Login
        },

    ]
}   

]
  
  // 3. 创建路由实例并传递 `routes` 配置
  // 你可以在这里输入更多的配置，但我们在这里
  // 暂时保持简单
const router = createRouter({
    // 4. 内部提供了 history 模式的实现。
    history: createWebHistory(''),
    routes // `routes: routes` 的缩写
  })

router.beforeEach((to, from,next) => {
    // ...
    // 返回 false 以取消导航
     if(to.path==="/login"||to.path==="/register")
       return next()

    const tokenStr=window.sessionStorage.getItem("token")
    if(!tokenStr||tokenStr!='admin'){
        return next("/login")
    }
    next()
    //return false
  })
export default router