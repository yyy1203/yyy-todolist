import { createApp } from 'vue'
import App from './App.vue'
//import ElementPlus from 'element-plus'
import vant from 'vant'
//import 'element-plus/dist/index.css'
import router from './router'

import store from './store'
import 'vant/lib/index.css';

createApp(App).use(router).use(store).use(vant).mount('#app')   //.use(ElementPlus, { size: 'small', zIndex: 3000 })
