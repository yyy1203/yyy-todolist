import request from '../../network/request'
export default function toDoFunc(todoLists, todoTitle) {
    const addTodo = (item) => {
        //debugger

        todoLists.value.push(item)
        todoTitle.value = ''
    }

    // 全部删除
    // const allDel=()=>{
    //     todoLists.value=''
    // }
    let originTodo = null;
    const editorTodo = (todo) => {
        originTodo = todo.title;
    }
    const cancelEditor = (todo) => {
        todo.title = originTodo
    }

    const delTodo = (todo) => {

        // console.log(todoLists.value) // 如果传过来是props.todoLists的话这个就没有值
        // const delIndex = todoLists.value.indexOf(todo)
        // todoLists.value.splice(delIndex, 1)
        //  todoLists.splice(todoLists.indexOf(todo),1)
        //  debugger
        const itemObj = {
            delid: todo.id
        }
        request.delItem(itemObj)
    }
    // 全部删除
    const delAll = () => {
        todoLists.value.length = 0;
        console.log('全部删除')
    }
    const updateItem=(data)=>{
        request.updateItem(data)
    }
    return {
        addTodo,
        editorTodo,
        cancelEditor,
        delTodo,
        delAll,
        updateItem
    }
}