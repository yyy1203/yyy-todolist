import {ref,watchEffect} from 'vue'
const LOCAL_KEY = "todolists"

function getTodoList() {
    let result = localStorage.getItem(LOCAL_KEY);
    if (result) {
        return JSON.parse(result)
    }
    return [];
}

function setTodoList(todos) {
    localStorage.setItem(LOCAL_KEY, JSON.stringify(todos));
}
export default function getTodoLists(){
    let todoLists = ref(getTodoList());
    watchEffect(()=>{
    //    debugger
        setTodoList(todoLists.value);
    })
  
    return {
        todoLists
    }
}