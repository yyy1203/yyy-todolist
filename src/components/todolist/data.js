import {
    ref,
    onMounted,
    onUnmounted,
    computed
} from 'vue'
import request from '../../network/request'


export default function getData() { //todoLists
    
    let getItems = async () => {
        const data = await request.getAllTodoItems()
        todoLists.value = data
    }
    onMounted(async () => {
        // const data = await request.getAllTodoItems()
        // todoLists.value = data
        getItems()
        // console.log(tabList.value[0].todoLists)
    })

    const todoLists = ref([])
    const todoTitle = ref('')

    const tabList = computed(() => {

        const activeTodoList = todoLists.value.filter(it => !it.completed);

        const finishedTodoList = todoLists.value.filter(it => it.completed);

        const activeBadge = activeTodoList.length == 0 ? '' : activeTodoList.length

        const completeBadge = finishedTodoList.length == 0 ? '' : finishedTodoList.length

        const activeTab = {
            badge: activeBadge,
            title: "Doning",
            datalist: activeTodoList,
            todoLists: todoLists.value
        }
        const compleTab = {
            badge: completeBadge,
            title: "Done",
            datalist: finishedTodoList,
            todoLists: todoLists.value
        }
        const data = []
        data.push(activeTab)
        data.push(compleTab)
        return data
    });





    return {
        todoLists,
        todoTitle,
        tabList
    }

}