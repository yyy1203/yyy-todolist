const LOCAL_KEY = "todolists"
export function getId() {
    return Date.now() + Math.random().toString(16).substr(2, 4);
}
export function getTodoList() {
    let result = localStorage.getItem(LOCAL_KEY);
    if (result) {
        return JSON.parse(result)
    }
    return [];
}

export function setTodoList(todos) {
    localStorage.setItem(LOCAL_KEY, JSON.stringify(todos));
}

export function filterTodoList(todos,visibility){
    if(visibility === 'all'){
        return todos
    }else if(visibility === 'active'){
        return todos.filter(it=>!it.completed)
    }else if(visibility === 'completed'){
        return todos.filter(it=>it.completed)
    }
}