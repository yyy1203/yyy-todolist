import {
  defineConfig
} from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // base: './',
  server: {
    port: 3001,
    // proxy: {
    //   '/api': {
    //     target: 'http://81.69.7.51:3010',
    //     changeOrigin: true,
    //     // rewrite:(path)=>path.replace(/^\/api/,'')
    //   }
    // }
  }

})