# yyy-todolist

#### 介绍
Vue3+Vite Todolist前端，需要配合后端代码使用 [todolist-node](https://gitee.com/yyy1203/todolist-node.git)
![输入图片说明](https://images.gitee.com/uploads/images/2021/1230/093718_b18c6b7e_7498667.png "屏幕截图.png")


#### 安装教程
因为用了Vite，所以需要较高版本的node才能运行使用

1.  npm i
2.  npm run dev
 登录用户名：admin
#### 使用说明

1.  需要修改network/request.js里面的地址
前端和后端都是本地直接npm run dev运行的话，就直接用localhost:后端运行端口就行了
如果是前端npm run build后把dist放服务器，后端是在服务器运行的话，就把下图的地方改成自己服务器的ip地址
![输入图片说明](https://images.gitee.com/uploads/images/2021/1230/094703_c3217cbf_7498667.png "屏幕截图.png")
